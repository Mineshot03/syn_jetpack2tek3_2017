/*
** EPITECH PROJECT, 2018
** PSU_myirc_2017
** File description:
** prepend
*/

#include <stdlib.h>
#include "list.h"

static int prepend(root_t *root, void *data)
{
	node_t *node;

	if ((node = malloc(sizeof(*node))) == NULL)
		return (1);
	node->data = data;
	node->next = root->first;
	node->previous = NULL;
	node->id = 1;
	root->first->previous = node;
	root->first = node;
	root->length += 1;
	node = root->first->next;
	while (node) {
		node->id += 1;
		node = node->next;
	}
	return (0);
}

static int prepend_empty(root_t *root, void *data)
{
	node_t *node;

	if ((node = malloc(sizeof(*node))) == NULL)
		return (1);
	node->next = NULL;
	node->previous = NULL;
	node->data = data;
	node->id = 1;
	root->length += 1;
	root->first = node;
	root->last = node;
	return (0);
}

int liblist_prepend(root_t *root, void *data)
{
	if (root->first == NULL)
		return (prepend_empty(root, data));
	else
		return (prepend(root, data));
}
