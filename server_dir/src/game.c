/*
** EPITECH PROJECT, 2018
** jetpack
** File description:
** game
*/

#include <stdio.h>
#include "server.h"

void send_start(void *data)
{
	client_t *client = (client_t *)data;

	dprintf(client->fd, "START\n");
}

int game_is_ready(server_t *serv)
{
	node_t *node = serv->clients->first;
	client_t *client;

	if (serv->fd_count != 3)
		return (0);
	while (node) {
		client = (client_t *)node->data;
		if (!client->is_ready)
			return (0);
		node = node->next;
	}
	return (1);
}

void send_pos(void *data)
{
	client_t *client = (client_t *)data;
	int fd = client->fd;
	node_t *node = client->server->clients->first;

	while (node) {
		client = (client_t *)node->data;
		dprintf(fd, "PLAYER %d %f %f %d\n", client->fd,
		client->x, client->server->map->height -
		client->y - 1, client->coins);
		node = node->next;
	}
}

void update_player(void *data)
{
	client_t *client = (client_t *)data;

	if (client->server->map->tab[(int)client->y][(int)client->x] == 'e')
		client->alive = 0;
	if (client->alive && client->x < client->server->map->width - 1)
		client->x += (float)1 / TICKRATE;
	if (client->y > 0 && client->fire)
		client->y -= (float)client->server->gravity * ((float)1 /
		TICKRATE);
	if (client->y < client->server->map->height - 1 && !client->fire)
		client->y += (float)client->server->gravity * ((float)1 /
		TICKRATE);
	if (client->server->map->tab[(int)client->y][(int)client->x] == 'c') {
		client->coins += 1;
		dprintf(client->fd, "COIN %d %f %f\n", client->fd,
		client->x, (float)client->server->map->height -
		client->y - 1.0f);
	}
}

int update_game(server_t *serv)
{
	static int start = 0;

	if (!start) {
		liblist_for_each(serv->clients, &send_start);
		start++;
	}
	if (game_has_ended(serv))
		return (1);
	liblist_for_each(serv->clients, &update_player);
	liblist_for_each(serv->clients, &send_pos);
	return (0);
}