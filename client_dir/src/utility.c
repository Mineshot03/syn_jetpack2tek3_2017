/*
** EPITECH PROJECT, 2018
** jetpack2Tek3
** File description:
** utility.c
*/

#include "client.h"

char **malloc_2d(int x, int y)
{
	char **tmp = malloc(sizeof(char *) * y);

	if (tmp == NULL)
		err("malloc");
	for (int i = 0; i != y; i++) {
		tmp[i] = calloc(sizeof(char), x);
		if (tmp[i] == NULL)
			err("calloc");
	}
	return (tmp);
}

void err(char *str)
{
	perror(str);
	exit(84);
}