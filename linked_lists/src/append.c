/*
** EPITECH PROJECT, 2018
** PSU_myirc_2017
** File description:
** append
*/

#include <stdlib.h>
#include "list.h"

static int append(root_t *root, void *data)
{
	node_t *node;

	if ((node = malloc(sizeof(*node))) == NULL)
		return (1);
	node->next = NULL;
	node->previous = root->last;
	node->data = data;
	node->id = root->last->id + 1;
	root->last->next = node;
	root->last = node;
	return (0);
}

static int append_empty(root_t *root, void *data)
{
	node_t *node;

	if ((node = malloc(sizeof(*node))) == NULL)
		return (1);
	node->id = 1;
	node->next = NULL;
	node->previous = NULL;
	node->data = data;
	root->first = node;
	root->last = node;
	root->length += 1;
	return (0);
}

int liblist_append(root_t *root, void *data)
{
	if (root->last == NULL)
		return (append_empty(root, data));
	else
		return (append(root, data));
}
