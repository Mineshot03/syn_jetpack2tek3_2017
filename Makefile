##
## EPITECH PROJECT, 2018
## my_irc
## File description:
## Makefile
##

MAKEFLAGS	+= 	--no-print-directory

all:
	@make -C linked_lists/
	@make -C client_dir/
	@make -C server_dir/

server:
	@make -C linked_lists/
	@make -C server_dir/

client:
	@make -C client_dir/

clean:
	@make -C linked_lists/ clean
	@make -C client_dir/ clean
	@make -C server_dir/ clean

fclean:
	@make -C linked_lists/ fclean
	@make -C client_dir/ fclean
	@make -C server_dir/ fclean

re: fclean all

.PHONY: re clean fclean all server client
