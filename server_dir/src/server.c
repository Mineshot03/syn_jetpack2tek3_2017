/*
** EPITECH PROJECT, 2018
** PSU_myirc_2017
** File description:
** create_server
*/

#include <unistd.h>
#include "server.h"

command_t command_tab_g[] = {{"ID", &handle_id}, {"MAP", &handle_map},
{"READY", &handle_ready}, {"FIRE", &handle_fire}, {NULL, NULL}};

map_t *create_map(char *path)
{
	map_t *map = malloc(sizeof(map_t));

	if (fill_map(map, path))
		return (map);
	return (NULL);
}

server_t *create_server(unsigned short port)
{
	server_t *serv = malloc(sizeof(server_t));

	if (!serv)
		return (NULL);
	serv->addr.sin_family = AF_INET;
	serv->addr.sin_port = htons(port);
	serv->addr.sin_addr.s_addr = INADDR_ANY;
	serv->fd_set[0].fd = socket(AF_INET, SOCK_STREAM, 0);
	serv->fd = serv->fd_set[0].fd;
	serv->fd_set[0].events = POLLIN;
	serv->fd_count = 1;
	if (serv->fd_set[0].fd < 0)
		return (NULL);
	if (bind(serv->fd_set[0].fd, (struct sockaddr *)&serv->addr,
	sizeof(serv->addr)) < 0)
		return (NULL);
	listen(serv->fd, 10);
	if ((serv->clients = liblist_create_list(NULL)) == NULL)
		return (NULL);
	return (serv);
}

int close_server(server_t *serv)
{
	if (!serv)
		return (0);
	return (!close(serv->fd));
}