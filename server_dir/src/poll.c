/*
** EPITECH PROJECT, 2018
** PSU_myirc_2017
** File description:
** poll
*/

#include <sys/ioctl.h>
#include <unistd.h>
#include <stdio.h>
#include "server.h"

void remove_fd(server_t *serv, int i)
{
	serv->fd_count--;
	for (int j = i; j < serv->fd_count; j++)
		serv->fd_set[j] = serv->fd_set[j + 1];
}

void remove_connection(server_t *serv, int *i)
{
	int bytes = -1;
	int fd = serv->fd_set[*i].fd;

	ioctl(serv->fd_set[*i].fd, FIONREAD, &bytes);
	if (bytes == 0 && serv->fd_set[*i].fd != serv->fd) {
		remove_client(serv, serv->fd_set[*i].fd);
		serv->fd_count--;
		for (int j = *i; j < serv->fd_count; j++)
			serv->fd_set[j] = serv->fd_set[j + 1];
		close(fd);
	} else if (bytes > 0)
		handle_message(serv->fd_set[*i].fd, serv);
}


void add_new_connection(server_t *serv)
{
	struct sockaddr_in cli_addr;
	unsigned int clilen = sizeof(cli_addr);
	int clifd;

	clifd = accept(serv->fd_set[0].fd,
	(struct sockaddr *) &cli_addr, &clilen);
	serv->fd_set[serv->fd_count].fd = clifd;
	serv->fd_set[serv->fd_count].events = POLLIN;
	serv->fd_count++;
	add_client(serv, clifd);
}

void poll_events(server_t *serv)
{
	if (game_is_ready(serv))
		if (update_game(serv))
			return ;
	poll(serv->fd_set, serv->fd_count, 200 / TICKRATE);
	for (int i = 1; i < serv->fd_count; i++) {
		if (serv->fd_set[i].revents & POLLIN)
			remove_connection(serv, &i);
	}
	if (serv->fd_set[0].revents & POLLIN && serv->fd_count < 3)
		add_new_connection(serv);
}