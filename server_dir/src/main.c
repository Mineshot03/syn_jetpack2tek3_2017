/*
** EPITECH PROJECT, 2018
** PSU_myirc_2017
** File description:
** main
*/

#include <stdio.h>
#include <string.h>
#include "server.h"

char *get_arg(int ac, char **av, char *arg)
{
	for (int i = 1; i < ac; i++) {
		if (!strcmp(av[i], arg) && i + 1 < ac)
			return av[i + 1];
	}

	return (NULL);
}

int usage(int ret)
{
	printf("USAGE: ./server -p port -g gravity -m map\n");
	return (ret);
}

int main(int ac, char **av)
{
	server_t *serv = NULL;
	unsigned short port;
	char *map = NULL;

	if (ac != 7)
		return (84);
	port = atoi(get_arg(ac, av, "-p"));
	map = get_arg(ac, av, "-m");
	serv = create_server(port);
	if (!serv) {
		perror("Error: ");
		return (84);
	}
	serv->gravity = atoi(get_arg(ac, av, "-g"));
	if ((serv->map = create_map(map)) == NULL)
		return (84);
	while (1)
		poll_events(serv);
	return (0);
}