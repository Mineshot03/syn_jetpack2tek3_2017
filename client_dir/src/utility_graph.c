/*
** EPITECH PROJECT, 2018
** jetpack2Tek3
** File description:
** utility_graph.c
*/

#include <SFML/Graphics.h>
#include "graphics.h"

sfVector2f vector2f(float x, float y)
{
	sfVector2f tmp = {x, y};

	return (tmp);
}

void set_view(sfRenderWindow *win, sfView *view, sfFloatRect rect,
sfFloatRect viewport)
{
	sfView_reset(view, rect);
	sfView_setViewport(view, viewport);
	sfRenderWindow_setView(win, view);
}