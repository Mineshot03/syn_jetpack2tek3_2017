/*
** EPITECH PROJECT, 2018
** jetpack2Tek3
** File description:
** render.c
*/

#include <stdlib.h>
#include <string.h>
#include "graphics.h"

static void render_map(graphics_t *app, game_t *game, int i)
{
	sfVector2f pos;

	for (int j = 0; game->size[0] >= j; j++) {
		pos.x = j * TILE;
		pos.y = i * TILE;
		sfSprite_setPosition(app->sprite->wall, pos);
		sfRenderWindow_drawSprite(app->win, app->sprite->wall, NULL);
		if (game->map[i][j] == 'c') {
			sfSprite_setPosition(app->sprite->coin, pos);
			sfRenderWindow_drawSprite(app->win, app->sprite->coin,
						NULL);
		} else if (game->map[i][j] == 'e') {
			sfSprite_setPosition(app->sprite->spike, pos);
			sfRenderWindow_drawSprite(app->win, app->sprite->spike,
						NULL);
		}
		if (game->size[0] == j) {
			sfSprite_setPosition(app->sprite->finish, pos);
			sfRenderWindow_drawSprite(app->win,
						app->sprite->finish, NULL);
		}
	}
}

static void render_text(graphics_t *app, char *str, int size, sfVector2f pos)
{
	sfText_setPosition(app->sprite->text, pos);
	sfText_setString(app->sprite->text, str);
	sfText_setCharacterSize(app->sprite->text, size);
	sfRenderWindow_drawText(app->win, app->sprite->text, NULL);
}

static void render_player(sfRenderWindow *win, sfSprite *player, int id,
			game_t *game)
{
	sfVector2f position = {(game->position[id][0]) * TILE,
			(game->size[1] - game->position[id][1] - 1) * TILE};

	if (id == 0)
		sfSprite_setColor(player, sfColor_fromRGB(0, 255, 0));
	else
		sfSprite_setColor(player, sfColor_fromRGB(255, 0, 0));
	sfSprite_setPosition(player, position);
	sfRenderWindow_drawSprite(win, player, NULL);
}

static void render_text_window(graphics_t *app, game_t *game)
{
	char tmp[100];

	if (game->status < 4)
		render_text(app, "Waiting for player 2!", 20,
			vector2f(10, 3));
	else {
		render_text(app, "YOU : ", 20, vector2f(10, 3));
		render_text(app, game->scoreP1, 20, vector2f(70, 3));
		render_text(app, "ENEMY : ", 20, vector2f(120, 3));
		render_text(app, game->scoreP2, 20, vector2f(210, 3));
		render_text(app, "Distance : ", 20, vector2f(260, 3));
		sprintf(tmp, "%d", (int)game->position[0][0]);
		render_text(app, tmp, 20, vector2f(360, 3));
	}
	if (game->finish == 1) {
		if (game->winner == game->id)
			render_text(app, "You win!", 20,
				vector2f(SCREENX / 2, 3));

		else if (game->winner == -1)
			render_text(app, "Nobody win!", 20,
				vector2f(SCREENX / 2, 3));
		else
			render_text(app, "You loose!", 20,
				vector2f(SCREENX / 2, 3));
	}
}

void render_window(graphics_t *app, game_t *game)
{
	sfFloatRect rect = {0, 0, SCREENX, TILE * (game->size[1] + 1)};

	sfRenderWindow_clear(app->win, sfBlack);
	if ((game->position[0][0] - 5) + 29 > game->size[0])
		rect.left = (game->size[0] - 29) * TILE;
	else if (game->position[0][0] > 5)
		rect.left = (game->position[0][0] - 5) * TILE;
	set_view(app->win, app->game, rect, (sfFloatRect) {0, 0.09, 1, 1});
	for (int i = 0; i < game->size[1]; i++)
		render_map(app, game, i);
	if (game->status == 4) {
		render_player(app->win, app->sprite->player, 1, game);
		render_player(app->win, app->sprite->player, 0, game);
	}
	set_view(app->win, app->score, (sfFloatRect) {0, 0, SCREENX, TILE},
		(sfFloatRect) {0, 0, 1, 0.1});
	render_text_window(app, game);
	sfRenderWindow_display(app->win);
}
