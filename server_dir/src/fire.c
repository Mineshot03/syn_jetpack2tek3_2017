/*
** EPITECH PROJECT, 2018
** jetpack
** File description:
** ready
*/

#include <stdio.h>
#include "server.h"

void handle_fire(char **command, server_t *serv, int fd)
{
	int length = tab_length(command);
	node_t *node = serv->clients->first;
	client_t *client;

	if (length != 2)
		return ;
	while (node) {
		client = (client_t *)node->data;
		if (client && client->fd == fd) {
			client->fire = atoi(command[1]);
			break ;
		}
		node = node->next;
	}
}