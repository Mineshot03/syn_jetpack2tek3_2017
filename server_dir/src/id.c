/*
** EPITECH PROJECT, 2018
** jetpack
** File description:
** id
*/

#include <stdio.h>
#include "server.h"

/* Takes no args, returns their fd */
void handle_id(char **command, server_t *serv, int fd)
{
	(void)command;
	(void)serv;
	dprintf(fd, "ID %d\n", fd);
}