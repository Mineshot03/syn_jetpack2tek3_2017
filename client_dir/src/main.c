/*
** EPITECH PROJECT, 2018
** jetpack2Tek3
** File description:
** main.c
*/

#include <pthread.h>
#include "client.h"

static network_t traitment_opt(int ac, char **av)
{
	int opt = 1;
	network_t addr;
	extern char *optarg;

	while (opt) {
		opt = getopt(ac, av, "h:p:");
		if (opt == -1)
			break;
		switch (opt) {
			case 'h':
				addr.ip = optarg;
				break;
			case 'p':
				addr.port = atoi(optarg);
				break;
			default:
				break;
		}
	}
	return (addr);
}

static game_t init_game_t(void)
{
	game_t game;
	game.id = 0;
	game.size[0] = 0;
	game.size[1] = 0;
	game.scoreP1 = "0";
	game.scoreP2 = "0";
	game.position[0][0] = 0;
	game.position[0][1] = 0;
	game.position[1][0] = 0;
	game.position[1][1] = 0;
	game.status = 0;
	game.finish = 0;
	game.fire = 0;
	game.winner = 0;
	return (game);
}

int main(int ac, char **av)
{
	if (ac < 5) {
		err("av");
	}
	network_t network = traitment_opt(ac, av);
	pthread_t graph;
	game_t game = init_game_t();
	if (pthread_create(&graph, NULL, graphics, &game) != 0)
		err("pthread_create");
	connect_server(&network, &game);
	if (pthread_join(graph, NULL) != 0)
		err("pthread_join");
	return (0);
}