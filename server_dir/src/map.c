/*
** EPITECH PROJECT, 2018
** jetpack
** File description:
** map
*/

#include <stdio.h>
#include <string.h>
#include "server.h"

int fill_map(map_t *map, char *path)
{
	FILE *stream = fopen(path, "r");
	char *buffer = NULL;
	size_t len = 0;
	ssize_t read;
	char *line;

	map->height = 0;
	map->tab = malloc(sizeof(char *));
	map->tab[0] = NULL;
	if (!stream)
		return (0);
	while ((read = getline(&buffer, &len, stream)) != -1) {
		map->width = strlen(buffer) - 1;
		line = strdup(buffer);
		line[strlen(line) - 1] = '\0';
		map->tab = add_to_tab(map->tab, line);
		map->height += 1;
	}
	map->str = tab_join(map->tab, '\0');
	return (1);
}

void handle_map(char **command, server_t *serv, int fd)
{
	(void)command;
	dprintf(fd, "MAP %d %d %s\n", serv->map->width, serv->map->height,
	serv->map->str);
}