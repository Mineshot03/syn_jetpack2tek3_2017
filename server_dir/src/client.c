/*
** EPITECH PROJECT, 2018
** PSU_myirc_2017
** File description:
** client
*/

#include "server.h"
#include "list.h"

int add_client(server_t *serv, int fd)
{
	client_t *client = malloc(sizeof(client_t));

	if (!client)
		return (1);
	client->fd = fd;
	client->is_ready = 0;
	client->fire = 0;
	client->coins = 0;
	client->x = 0.0f;
	client->y = (float)(serv->map->height / 2);
	client->server = serv;
	client->alive = 1;
	return (liblist_append(serv->clients, client));
}

int norme(server_t *serv, node_t *node)
{
	if (node->previous)
		node->previous->next = node->next;
	if (serv->clients->first == node)
		serv->clients->first = node->next;
	if (node->next)
		node->next->previous = node->previous;
	if (serv->clients->last == node)
		serv->clients->last = node->previous;
	free(node);
	return (0);
}

int remove_client(server_t *serv, int fd)
{
	client_t *client;
	node_t *node = serv->clients->first;

	while (node) {
		client = (client_t *)node->data;
		if (client->fd == fd) {
			return (norme(serv, node));
		}
		node = node->next;
	}
	return (0);
}