/*
** EPITECH PROJECT, 2018
** PSU_myirc_2017
** File description:
** list
*/

#ifndef LIST_H_
# define LIST_H_

typedef struct		node_s
{
	void			*data;
	int			id;
	struct node_s		*next;
	struct node_s		*previous;
}			node_t;

typedef struct		root_s
{
	int			length;
	void			(*custom_free)(void *);
	node_t			*first;
	node_t			*last;
}			root_t;

int		print_error(char *);
int		print_add_error();

root_t		*liblist_create_list(void (*)(void *));
int		liblist_destroy_list(root_t *, int);
int		liblist_append(root_t *, void *);
int		liblist_prepend(root_t *, void *);
int		liblist_print_list(root_t *, char *);
void		liblist_delete_head(root_t *);
void		liblist_delete_tail(root_t *);
void		liblist_for_each(root_t *, void (*)(void *));

#endif /* !LIST_H_ */
