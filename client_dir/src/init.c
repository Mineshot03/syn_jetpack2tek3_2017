/*
** EPITECH PROJECT, 2018
** jetpack2Tek3
** File description:
** init.c
*/

#include "graphics.h"

sfRenderWindow *new_window(game_t *game)
{
	while (game->status < 3)
		usleep(1);
	sfVideoMode mode = {SCREENX, TILE * (game->size[1] + 1), 32};
	sfRenderWindow *win = sfRenderWindow_create(mode, "JetPack 2",
	sfClose, NULL);
	return (win);
}

void create_sprite(sprite_t *sprite)
{
	sprite->coin = sfSprite_create();
	link_texture(sprite->coin, TEXTURE_FILEPATH,
	(sfIntRect) {0, 0, 32, 32});
	sprite->spike = sfSprite_create();
	link_texture(sprite->spike, TEXTURE_FILEPATH,
	(sfIntRect) {0, 32, 32, 32});
	sprite->player = sfSprite_create();
	link_texture(sprite->player, TEXTURE_FILEPATH,
	(sfIntRect) {0, 64, 32, 32});
	sprite->wall = sfSprite_create();
	link_texture(sprite->wall, TEXTURE_FILEPATH,
	(sfIntRect) {0, 96, 32, 32});
	sprite->finish = sfSprite_create();
	link_texture(sprite->finish, TEXTURE_FILEPATH,
	(sfIntRect) {0, 128, 32, 32});
}

void link_texture(sfSprite *sprite, char *file, sfIntRect rect)
{
	sfSprite_setTexture(sprite,
	sfTexture_createFromFile(file, &rect), sfTrue);
}

void create_text(sprite_t *sprite)
{
	sprite->arial = sfFont_createFromFile(FONT_FILEPATH);
	sprite->text = sfText_create();
	sfText_setFont(sprite->text, sprite->arial);
	sfText_setCharacterSize(sprite->text, 20);
}

void create_view(graphics_t *app, game_t *game)
{
	app->game = sfView_create();
	sfView_setSize(app->game, (sfVector2f) {game->size[0] * TILE,
	game->size[1] * TILE});
	app->score = sfView_create();
	sfView_setSize(app->game, (sfVector2f) {SCREENX, TILE});
}