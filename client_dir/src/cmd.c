/*
** EPITECH PROJECT, 2018
** jetpack2Tek3
** File description:
** cmd.c
*/

#include <string.h>
#include "client.h"

static void cmd_player(game_t *game)
{
	if (game->id == atoi(strtok(NULL, " "))) {
		game->position[0][0] = atof(strtok(NULL, " "));
		game->position[0][1] = atof(strtok(NULL, " "));
		game->scoreP1 = strtok(NULL, " ");
	} else {
		game->position[1][0] = atof(strtok(NULL, " "));
		game->position[1][1] = atof(strtok(NULL, " "));
		game->scoreP2 = strtok(NULL, " ");
	}
}

static void cmd_map(game_t *game)
{
	char *tmp;

	game->size[0] = atoi(strtok(NULL, " "));
	game->size[1] = atoi(strtok(NULL, " "));
	game->map = malloc_2d(game->size[0] + 1, game->size[1]);
	tmp = strtok(NULL, " ");
	for (int i = 0; i != game->size[1]; i++) {
		memcpy(game->map[i], tmp, game->size[0]);
		tmp = &tmp[game->size[0]];
	}
	game->status = 2;
}

static void cmd_coin(game_t *game)
{
	int x;
	int y;

	strtok(NULL, " ");
	x = atoi(strtok(NULL, " "));
	y = atoi(strtok(NULL, " "));
	game->map[game->size[1] - y - 1][x] = '_';
}

static void cmd_finish(game_t *game)
{
	game->winner = atoi(strtok(NULL, " "));

	if (game->winner == -1)
		printf("Nobody wins\n");
	else if (game->winner == game->id)
		printf("You win!\n");
	else
		printf("You loose!\n");
	game->finish = 1;
}

void traitment_cmd(network_t *network, game_t *game)
{
	char *cmd = strtok(network->str, " ");

	if (!strcasecmp(cmd, "FINISH"))
		cmd_finish(game);
	else if (!strcasecmp(cmd, "ID")) {
		game->id = atoi(strtok(NULL, " "));
		game->status = 1;
	} else if (!strcasecmp(cmd, "MAP"))
		cmd_map(game);
	else if (!strcasecmp(cmd, "PLAYER"))
		cmd_player(game);
	else if (!strcasecmp(cmd, "COIN"))
		cmd_coin(game);
	else if (!strcasecmp(cmd, "START"))
		game->status = 4;
}