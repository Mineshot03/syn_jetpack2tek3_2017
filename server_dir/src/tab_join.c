/*
**EPITECHPROJECT,2018
**PSU_myirc_2017
**Filedescription:
**tab_join
*/

#include <string.h>
#include"server.h"

static int count_size(char**tab)
{
	int i = 0;
	int result = 0;

	while (tab[i]) {
		result += strlen(tab[i]);
		i++;
	}
	return (result + i);
}

char *tab_join(char **tab, char c)
{
	int i = 0;
	int j = 0;
	int k = 0;
	char *result = malloc(sizeof(char) * (count_size(tab) + 1));

	if (result == NULL)
		return (NULL);
	while (tab[i]) {
		j = 0;
		while (tab[i][j])
			result[k++] = tab[i][j++];
		if (tab[i + 1] != NULL && c != 0)
			result[k++] = c;
		i++;
	}
	result[k] = '\0';
	return (result);
}
