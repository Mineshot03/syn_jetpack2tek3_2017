/*
** EPITECH PROJECT, 2018
** jetpack2Tek3
** File description:
** network.c
*/

#include <string.h>
#include <arpa/inet.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <sys/select.h>
#include <sys/time.h>
#include <netdb.h>
#include "client.h"

static char *receve(int sock)
{
	char *str = malloc(sizeof(char *) * 1);
	char c;
	int i = 0;
	int ret;

	if (str == NULL)
		err("malloc");
	while (1) {
		ret = read(sock, &c, 1);
		if (ret == -1)
			err("read");
		if (c == '\n')
			break;
		else if (c == '\0')
			exit(0);
		str[i] = c;
		i++;
		str = realloc(str, sizeof(char *) * i + 1);
	}
	str[i] = '\0';
	return str;
}

static void check_sock(network_t *network, game_t *game)
{
	struct timeval tv;
	tv.tv_sec = 0;
	tv.tv_usec = 1;
	fd_set readfd;
	FD_ZERO(&readfd);
	FD_SET(network->sock, &readfd);
	select(network->sock + 1, &readfd, NULL, NULL, &tv);
	if (FD_ISSET(network->sock, &readfd)) {
		network->str = receve(network->sock);
		printf("%s\n", network->str);
		traitment_cmd(network, game);
	}
}

static void game_check(network_t *addr, game_t *game)
{
	static int old = 0;

	if (game->status == 2) {
		if (send(addr->sock, "READY\n", strlen("ready\n"), 0) == -1)
			err("send");
		game->status++;
	}
	if (game->fire == 1 && old == 0) {
		old = 1;
		if (send(addr->sock, "FIRE 1\n", strlen("FIRE 1\n"), 0) == -1)
			err("send");
	} else if (game->fire == 0 && old == 1) {
		old = 0;
		if (send(addr->sock, "FIRE 0\n", strlen("FIRE 0\n"), 0) == -1)
			err("send");
	}
}

static void communication_server(network_t *addr, game_t *game)
{
	while (1) {
		if (game->status == 0)
			if (send(addr->sock, "ID\n", strlen("id\n"), 0) == -1)
				err("send");
		if (game->status == 1)
			if (send(addr->sock, "MAP\n", strlen("map\n"), 0) == -1)
				err("send");
		check_sock(addr, game);
		if (game->finish == 0)
			game_check(addr, game);
		else
			break;
	}
}

void connect_server(network_t *addr, game_t *game)
{
	addr->sock = socket(AF_INET, SOCK_STREAM, 0);
	struct sockaddr_in dest;
	struct hostent *host = NULL;

	memset(&dest, 0, sizeof(dest));
	dest.sin_family = AF_INET;
	host = gethostbyname(addr->ip);
	dest.sin_addr = *(struct in_addr *) host->h_addr;
	dest.sin_port = htons(addr->port);
	if (connect(addr->sock, (struct sockaddr *) &dest,
	sizeof(struct sockaddr_in)) == -1)
		err("connect");
	communication_server(addr, game);
}