/*
** EPITECH PROJECT, 2018
** jetpack2Tek3
** File description:
** client.h
*/

#ifndef CLIENT_H_
#define CLIENT_H_

#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>

struct network_s {
	char *ip;
	int port;
	int sock;
	char *str;
};
typedef struct network_s network_t;

struct game_s {
	int id;
	int size[2];
	char **map;
	char *scoreP1;
	char *scoreP2;
	float position[2][2];
	int status;
	int finish;
	int winner;
	int fire;
};
typedef struct game_s game_t;

void connect_server(network_t *, game_t *);
void traitment_cmd(network_t *, game_t *);
char **malloc_2d(int, int);
void err(char *);
void *graphics(void *);

#endif /* !CLIENT_H_ */
