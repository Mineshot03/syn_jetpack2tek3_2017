/*
** EPITECH PROJECT, 2018
** jetpack
** File description:
** end
*/

#include <stdio.h>
#include "server.h"

void no_winner(void *data)
{
	client_t *client = (client_t *)data;

	dprintf(client->fd, "FINISH -1\n");
}

void send_winner(int id, int fd, int fd2)
{
	dprintf(fd, "FINISH %d\n", id);
	dprintf(fd2, "FINISH %d\n", id);
}

int game_has_ended(server_t *serv)
{
	client_t *client1 = (client_t *)serv->clients->first->data;
	client_t *client2 = (client_t *)serv->clients->last->data;
	int width = client1->server->map->width;

	if (!client1->alive && !client2->alive) {
		liblist_for_each(serv->clients, &no_winner);
		return (1);
	} else if (!client1->alive && client2->alive) {
		send_winner(client2->fd, client1->fd, client2->fd);
		return (1);
	} else if (!client2->alive && client1->alive) {
		send_winner(client1->fd, client1->fd, client2->fd);
		return (1);
	} else if ((int)client2->x == width - 1 && (int)client1->x ==
	width - 1) {
		send_winner(client1->coins > client2->coins ? client1->fd :
		client2->fd, client1->fd, client2->fd);
		return (1);
	}
	return (0);
}