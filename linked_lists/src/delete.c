/*
** EPITECH PROJECT, 2018
** PSU_myirc_2017
** File description:
** delete
*/

#include <stdlib.h>
#include "list.h"

void liblist_delete_head(root_t *root)
{
	node_t *node;

	if (root->first == NULL)
		return ;
	node = root->first;
	root->first = root->first->next;
	if (root->first != NULL)
		root->first->previous = NULL;
	if (root->length == 1)
		root->last = NULL;
	if (root->custom_free != NULL)
		root->custom_free(node->data);
	else
		free(node->data);
	free(node);
	node = root->first;
	while (node) {
		node->id -= 1;
		node = node->next;
	}
	root->length -= 1;
}

void liblist_delete_tail(root_t *root)
{
	node_t *node;

	if (root->last == NULL)
		return ;
	node = root->last;
	if (node->previous != NULL)
		root->last = node->previous;
	root->last->next = NULL;
	if (root->length == 1)
		root->first = NULL;
	if (root->custom_free != NULL)
		root->custom_free(node->data);
	else
		free(node->data);
	free(node);
	root->length -= 1;
}
