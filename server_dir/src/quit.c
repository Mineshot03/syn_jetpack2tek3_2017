/*
** EPITECH PROJECT, 2018
** PSU_myirc_2017
** File description:
** quit
*/

#include <stdio.h>
#include <unistd.h>
#include "server.h"

int get_index(int fd, server_t *serv)
{
	for (int i = 1; i < serv->fd_count; i++) {
		if (serv->fd_set[i].fd == fd)
			return (i);
	}
	return (-1);
}

void remove_c(int fd, server_t *serv)
{
	int index = get_index(fd, serv);

	if (index < 0)
		return ;
	remove_client(serv, fd);
	remove_fd(serv, index);
	close(fd);
}