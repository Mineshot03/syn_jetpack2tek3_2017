/*
** EPITECH PROJECT, 2018
** PSU_myirc_2017
** File description:
** message
*/

#include <unistd.h>
#include <string.h>
#include <stdio.h>
#include "server.h"

void handle_command(char *command, server_t *serv, int fd)
{
	char **tab = str_to_wordtab(command, " ");

	if (!tab)
		return ;
	for (int i = 0; tab[i] != NULL; i++) {
		printf("%s\n", tab[i]);
	}
	printf("\n");
	for (int i = 0; command_tab_g[i].command != NULL; i++)
		if (!strcmp(command_tab_g[i].command, tab[0]))
			command_tab_g[i].action(tab, serv, fd);
}

void handle_message(int fd, server_t *serv)
{
	char buffer[512];
	char **tab = NULL;
	int readc = read(fd, buffer, 512);

	if (readc <= 0)
		return ;
	buffer[readc] = '\0';
	tab = str_to_wordtab(buffer, "\n");
	if (!tab)
		return ;
	for (int i = 0; tab[i] != NULL; i++) {
		if (tab[i][strlen(tab[i]) - 1] == '\r')
			tab[i][strlen(tab[i]) - 1] = '\0';
		handle_command(tab[i], serv, fd);
	}
}