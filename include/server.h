/*
** EPITECH PROJECT, 2018
** PSU_myirc_2017
** File description:
** server
*/

#ifndef SERVER_H_
	#define SERVER_H_

#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdlib.h>
#include <poll.h>
#include "list.h"

#define TICKRATE 8

typedef struct map_s
{
	char **tab;
	char *str;
	int width;
	int height;
} map_t;

typedef struct server_s
{
	struct sockaddr_in addr;
	struct pollfd fd_set[1024];
	int fd;
	int fd_count;
	int gravity;
	map_t *map;
	root_t *clients;
} server_t;

typedef struct client_s
{
	int fd;
	int is_ready;
	int alive;
	int fire;
	int coins;
	float x;
	float y;
	server_t *server;
} client_t;

typedef struct command_s
{
	char *command;
	void (*action)(char **, server_t *, int);
} command_t;

server_t *create_server(unsigned short port);
int close_server(server_t *serv);
void poll_events(server_t *serv);
int add_client(server_t *serv, int fd);
int remove_client(server_t *serv, int fd);
void handle_message(int fd, server_t *);
char **str_to_wordtab(char *str, char *delimiter);
int tab_length(char **tab);
char **add_to_tab(char **tab, char *str);
char **handle_prefix(char **tab);
int is_identified(server_t *serv, int fd);
void send_ping(int fd, server_t *serv);
void remove_fd(server_t *serv, int i);
void remove_c(int fd, server_t *serv);
int add_channel(int fd, server_t *serv, char *channel);
void leave_channel(int fd, server_t *serv, char *channel);
char *get_nick(int fd, server_t *serv);
char *tab_join(char **tab, char c);
void alert_channels(int fd, char *nick, server_t *serv, char *message);
void alert_channel(char *channel, char *nick, server_t *serv, char *message);
void set_nick(int fd, char *nick, server_t *serv);
char *get_nick(int fd, server_t *serv);
int is_in_use(int fd, server_t *serv, char *nick);
int get_index(int fd, server_t *serv);
int fill_map(map_t *map, char *path);
map_t *create_map(char *path);
int update_game(server_t *serv);
int game_is_ready(server_t *serv);
int game_has_ended(server_t *serv);

/* handlers */
void handle_id(char **command, server_t *serv, int fd);
void handle_map(char **command, server_t *serv, int fd);
void handle_ready(char **command, server_t *serv, int fd);
void handle_fire(char **command, server_t *serv, int fd);

extern command_t command_tab_g[];

#endif /* !SERVER_H_ */
