/*
** EPITECH PROJECT, 2018
** PSU_myirc_2017
** File description:
** better_wordtab
*/

#include <string.h>
#include <stdlib.h>

int tab_length(char **tab)
{
	int i = 0;

	while (tab[i])
		i++;
	return (i);
}

char **add_to_tab(char **tab, char *str)
{
	char **result;
	int i;

	i = tab_length(tab);
	if ((result = malloc(sizeof(char *) * (i + 2))) == NULL)
		return (NULL);
	i = 0;
	while (tab[i]) {
		result[i] = tab[i];
		i++;
	}
	result[i++] = str;
	result[i] = 0;
	free(tab);
	return (result);
}

char *realloc_size(char *str)
{
	char *tmp;

	tmp = strdup(str);
	free(str);
	return (tmp);
}

char **str_to_wordtab(char *str, char *delimiter)
{
	char **result = malloc(sizeof(char *));
	char *tmp;
	char *dup = strdup(str);

	if (!str || !result)
		return (NULL);
	result[0] = 0;
	if (dup == NULL)
		return (NULL);
	while ((tmp = strsep(&dup, delimiter)) != NULL)
		if (tmp[0] != '\0')
			result = add_to_tab(result, strdup(tmp));
	return (result);
}