/*
** EPITECH PROJECT, 2018
** PSU_myirc_2017
** File description:
** for_each
*/

#include <stdlib.h>
#include "list.h"

void liblist_for_each(root_t *root, void (*function)(void *))
{
	node_t *node;

	node = root->first;
	while (node) {
		function(node->data);
		node = node->next;
	}
}