/*
** EPITECH PROJECT, 2018
** jetpack2Tek3
** File description:
** graphics.c
*/

#include "graphics.h"

static void get_event(graphics_t *app, game_t *game)
{
	sfRenderWindow_pollEvent(app->win, &app->event);
	switch (app->event.type) {
		case sfEvtClosed:
			sfRenderWindow_close(app->win);
			break;
		case sfEvtKeyPressed:
			if (app->event.key.code == sfKeySpace &&
			game->fire == 0)
				game->fire = 1;
			if (app->event.key.code == sfKeyEscape)
				sfRenderWindow_close(app->win);
			break;
		case sfEvtKeyReleased:
			if (app->event.key.code == sfKeySpace &&
			game->fire == 1)
				game->fire = 0;
			break;
		default:
			break;
	}
}

static void csfml_destroy(graphics_t *app)
{
	sfSprite_destroy(app->sprite->coin);
	sfSprite_destroy(app->sprite->spike);
	sfSprite_destroy(app->sprite->player);
	sfSprite_destroy(app->sprite->wall);
	sfSprite_destroy(app->sprite->finish);
	sfText_destroy(app->sprite->text);
	sfFont_destroy(app->sprite->arial);
	sfView_destroy(app->game);
	sfView_destroy(app->score);
	sfRenderWindow_destroy(app->win);
	free(app->sprite);
}

void *graphics(void *data)
{
	game_t *game = (game_t *) data;
	graphics_t app;

	app.movement = 0.0;
	app.sprite = malloc(sizeof(sprite_t));
	if (app.sprite == NULL)
		err("malloc");
	app.win = new_window(game);
	create_view(&app, game);
	create_sprite(app.sprite);
	create_text(app.sprite);
	while (sfRenderWindow_isOpen(app.win)) {
		get_event(&app, game);
		if (game->finish == 0)
			render_window(&app, game);
	}
	csfml_destroy(&app);
	exit(0);
}