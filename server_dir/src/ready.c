/*
** EPITECH PROJECT, 2018
** jetpack
** File description:
** ready
*/

#include <stdio.h>
#include "server.h"

void handle_ready(char **command, server_t *serv, int fd)
{
	node_t *node = serv->clients->first;
	client_t *client;

	while (node) {
		client = (client_t *)node->data;
		if (client && client->fd == fd) {
			client->is_ready = 1;
			break ;
		}
		node = node->next;
	}
	(void)command;
}