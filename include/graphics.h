/*
** EPITECH PROJECT, 2018
** jetpack2Tek3
** File description:
** graphics.h
*/

#ifndef TMP_GRAPHICS_H
#define TMP_GRAPHICS_H

# include <SFML/Graphics.h>
# include "client.h"

#define TILE 32
#define SCREENX	960
#define TEXTURE_FILEPATH "assets/textures.png"
#define FONT_FILEPATH "assets/arial.ttf"

struct sprite_s {
	sfSprite *coin;
	sfSprite *spike;
	sfSprite *wall;
	sfSprite *player;
	sfSprite *finish;
	sfFont *arial;
	sfText *text;
};
typedef struct sprite_s sprite_t;

struct graphics_s {
	sfRenderWindow *win;
	sfEvent event;
	sfView *game;
	sfView *score;
	float movement;
	sprite_t *sprite;
};
typedef struct graphics_s graphics_t;

sfVector2f vector2f(float, float);
void set_view(sfRenderWindow *, sfView *, sfFloatRect, sfFloatRect);
void render_window(graphics_t *, game_t *);
sfRenderWindow *new_window(game_t *);
void create_sprite(sprite_t *);
void create_text(sprite_t *);
void create_view(graphics_t *, game_t *);
void link_texture(sfSprite *, char *, sfIntRect);

#endif //TMP_GRAPHICS_H
