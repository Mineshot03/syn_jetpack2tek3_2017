/*
** EPITECH PROJECT, 2018
** PSU_myirc_2017
** File description:
** list
*/

#include <stdlib.h>
#include "list.h"

root_t *liblist_create_list(void (*custom_free)(void *))
{
	root_t *result;

	if ((result = malloc(sizeof(*result))) == NULL)
		return (NULL);
	result->length = 0;
	result->first = NULL;
	result->last = NULL;
	result->custom_free = custom_free;
	return (result);
}

int liblist_destroy_list(root_t *root, int free_root)
{
	node_t *tmp;

	while (root->first) {
		tmp = root->first;
		root->first = tmp->next;
		if (root->custom_free == NULL)
			free(tmp->data);
		else
			root->custom_free(tmp->data);
		free(tmp);
	}
	if (free_root)
		free(root);
	return (0);
}
